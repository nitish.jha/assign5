terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"
}

# Modules 
module "aws_vpc" {
  source    = "./modules/vpc"
  vpc_cidr  = var.cidr
  ipv6_cidr = var.ipv6_cidr_block
  vpc_name  = var.vpc_name
  vpc_tag   = var.vpc_tag
  tenancy   = var.tenancy_status
}

module "aws_subnet" {
  source              = "./modules/subnet"
  id_vpc              = module.aws_vpc.vpcid.id
  subnet_cidr_block   = var.subnet_cidr
  public_ip_on_launch = var.public_ip_on_launch
  availability_zone   = var.availability_zone
  subnet_name         = var.subnet_name
  subnet_tag          = var.subnet_tag

}

